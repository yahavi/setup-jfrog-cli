FROM node:10-alpine

RUN apk add --update --no-cache jq bash curl wget git

COPY pipe /usr/bin/

ENTRYPOINT ["sh", "/usr/bin/pipe.sh"]